---
layout: post
title:  "First post in a disastrous year"
date:   2020-10-04
categories: jekyll update
---
# Maybe this is ok?

For the creator of sideweb as to not be confused:
- make new post in `_posts` directory
- format the title to the date \+ title (preferably the current date, expect in 2020)
- run `bundle exec jekyll serve` to look at the changes
- more info: [Jekyll docs][jekyll-docs]

Relax, rejoice, wait for 2021...
Maybe post a bit more.

EDIT: Need to edit this maybe to fix errors after a project path change. Let's see (this is how I work on errors. With trial and more error...)
 
[jekyll-docs]: http://jekyllrb.com/docs/home
